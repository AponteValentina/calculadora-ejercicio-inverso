function calcular(){
    let X=Number(document.getElementById('coordenadaX').value);
    let Y=Number(document.getElementById('coordenadaY').value);
    let Z=Number(document.getElementById('coordenadaZ').value);
    let a,b,f,eCua,Long, Long_G,Long_M, Long_S, Lat_0, Lat_0_G,Lat_0_M,Lat_0_S, N1, Lat_1, Lat_1_G,Lat_1_M,Lat_1_S, h;
 
    a=6378137;
    b=6356752,3141;
    f=1/298.2572223563;
    eCua=(2*(f))-Math.pow((f),2);



    //Calculo de la Longitud Geodésica

    Long= 2* ((Math.atan((Y/(X+Math.sqrt(Math.pow(X, 2)+Math.pow(Y, 2)))))) *(180/Math.PI));
    document.getElementById('Longitud').value=Long;

    Long_G= Math.trunc(Long);
    document.getElementById('Longitud_G').value=Long_G;

    Long_M= (Math.abs(Long) - (Math.abs(Math.trunc(Long))))*60;
    document.getElementById('Longitud_M').value=Math.trunc(Long_M);

    Long_S= (Long_M - Math.trunc(Long_M))*60;
    document.getElementById('Longitud_S').value=Long_S;

    //Calculo de la Latitud Geodésica
    
    Lat_0=(Math.atan((Z/(Math.sqrt(Math.pow(X, 2)+Math.pow(Y, 2))))*(1+(eCua/(1-eCua)))))*(180/Math.PI); 
    document.getElementById('Latitud_0').value= Lat_0;
    
    Lat_0_G= Math.trunc(Lat_0);
    document.getElementById('Latitud_0_G').value=Lat_0_G;

    Lat_0_M= (Math.abs(Lat_0) - (Math.abs(Math.trunc(Lat_0))))*60;
    document.getElementById('Latitud_0_M').value=Math.trunc(Lat_0_M);

    Lat_0_S= (Lat_0_M - Math.trunc(Lat_0_M))*60;
    document.getElementById('Latitud_0_S').value=Lat_0_S;

    //Calculo de N1
   
    N1=a/(Math.sqrt((1-(eCua*(Math.pow((Math.sin(Lat_0*(Math.PI/180))),2)))),2));
    document.getElementById('N1').value=N1;

    //Calculo latitud 1

    Lat_1= (Math.atan((Z+(eCua*N1*Math.sin(Lat_0*(Math.PI/180))))/(Math.sqrt(Math.pow(X, 2)+Math.pow(Y, 2)))))*(180/Math.PI);
    document.getElementById('Latitud_1').value= Lat_1;

    Lat_1_G= Math.trunc(Lat_1);
    document.getElementById('Latitud_1_G').value=Lat_1_G;

    Lat_1_M= (Math.abs(Lat_1) - (Math.abs(Math.trunc(Lat_1))))*60;
    document.getElementById('Latitud_1_M').value=Math.trunc(Lat_1_M);

    Lat_1_S= (Lat_1_M - Math.trunc(Lat_0_M))*60;
    document.getElementById('Latitud_1_S').value=Lat_1_S;

    //Calculo de la altura

    h=((Math.sqrt(Math.pow(X, 2)+Math.pow(Y, 2)))/Math.cos(Lat_0*(Math.PI/180)))-N1;
    document.getElementById('h').value=h;

}

function limpiar(){
    document.getElementById('coordenadaX').value="";
    document.getElementById('coordenadaY').value="";
    document.getElementById('Longitud').value="";
    document.getElementById('Longitud_G').value="";
    document.getElementById('Longitud_M').value="";
    document.getElementById('Longitud_S').value="";
    document.getElementById('coordenadaZ').value="";
    document.getElementById('Latitud_0').value= "";
    document.getElementById('Latitud_0_G').value="";
    document.getElementById('Latitud_0_M').value="";
    document.getElementById('Latitud_0_S').value="";
    document.getElementById('N1').value="";
    document.getElementById('Latitud_1').value= "";
    document.getElementById('Latitud_1_G').value="";
    document.getElementById('Latitud_1_M').value="";
    document.getElementById('Latitud_1_S').value="";
    document.getElementById('h').value="";
}

